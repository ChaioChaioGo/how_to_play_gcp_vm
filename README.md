# How_To_Play_GCP_VM_Linux_Deploy_Apache

(Linux Ubuntu 作業系統篇) GCP 虛擬機操作筆記

## Getting started

選擇虛擬機 E2 + Ubuntu 1804

## 打開虛擬機(ssh)

- [ ] 首下更新 `sudo apt update `
- [ ] 安裝 MySQL 資料庫輸入以下指令 `sudo apt install mysql-server`
- [ ] 確定 MySQL 服務指令 `sudo service MySQL status`

## 安裝 PhpMyAdmin(來用網頁服務介面操控資料庫)

- [ ] 安裝 `sudo apt update && sudo apt install phpmyadmin`

下完指令安裝後會需要設定預設值，密碼就自行定義

- [ ] 將它加入 Apache 設置 `sudo vim /etc/apache2/apache2.conf`

Linux 按下`i`之後可修改內容，請於最後一行加入`Include /etc/phpmyadmin/apache.conf`

`esc`取消編輯後 `:wq` 存檔後離開

- [ ] 重啟伺服器 `sudo service apache2 restart`

ip: xxx.xxx.xxx.xxx/phpmyadmin 就可以操作資料庫囉

## 安裝 docker 

- [ ] `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

- [ ] `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

- [ ] `sudo apt-get update`

- [ ] 免費版本的Docker CE `sudo apt-get install -y docker-ce`
- [ ] 啟動 `sudo systemctl status docker`

  active (running)  => 運作中

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

